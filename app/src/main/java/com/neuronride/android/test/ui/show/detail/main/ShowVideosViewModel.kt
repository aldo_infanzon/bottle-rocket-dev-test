package com.neuronride.android.test.ui.show.detail.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.neuronride.android.test.data.container.show.Show
import com.neuronride.android.test.data.container.video.Video
import com.neuronride.android.test.repository.DefaultDatabase
import io.reactivex.Flowable

class ShowVideosViewModel(app: Application) : AndroidViewModel(app) {

    private val showsDao = DefaultDatabase
        .getInstance(app)
        .showDao()

    private val videosDao = DefaultDatabase
        .getInstance(app)
        .videoDao()

    fun getShow(id: Long): Flowable<Show> {
        return showsDao.findById(id)
    }

    fun getAllVideos(showId: Long): Flowable<List<Video>> {
        return videosDao.getAllByShow(showId)
    }

}