package com.neuronride.android.test.repository

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.neuronride.android.test.data.container.ad.VideoAd
import com.neuronride.android.test.data.container.ad.VideoAdDao
import com.neuronride.android.test.data.container.image.Image
import com.neuronride.android.test.data.container.image.ImageDao
import com.neuronride.android.test.data.container.show.Show
import com.neuronride.android.test.data.container.show.ShowDao
import com.neuronride.android.test.data.container.video.Video
import com.neuronride.android.test.data.container.video.VideoDao
import com.neuronride.android.test.data.request.ShowsRequestModel
import io.reactivex.Single


const val VERSION = 1

const val NAME = "default-database"

@Database(
    entities = [
        Show::class,
        Video::class,
        VideoAd::class,
        Image::class
    ],
    version = VERSION
)
abstract class DefaultDatabase : RoomDatabase() {

    companion object {

        @Volatile
        private var INSTANCE: DefaultDatabase? = null

        fun getInstance(context: Context): DefaultDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE ?: buildDatabase(context).also { db ->
                    INSTANCE = db
                }
            }

        /**
         * Dummy fill db function, this must be done against a service
         */
        fun fillDb(context: Context, showsRequestModel: ShowsRequestModel): Single<Boolean> {
            val showList = showsRequestModel.shows

            return Single.create<Boolean> {
                getInstance(context).showDao()
                    .insert(*showList!!.toTypedArray())

                for (show in showList) {
                    getInstance(context).videoDao()
                        .insert(*show.videos.toTypedArray())

                    getInstance(context).videoAdDao()
                        .insert(*show.videoAds.toTypedArray())

                    getInstance(context).imageDao()
                        .insert(*show.images.toTypedArray())
                }

                it.onSuccess(true)
            }

        }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                DefaultDatabase::class.java, NAME
            ).build()
    }

    abstract fun showDao(): ShowDao

    abstract fun videoDao(): VideoDao

    abstract fun videoAdDao(): VideoAdDao

    abstract fun imageDao(): ImageDao

}
