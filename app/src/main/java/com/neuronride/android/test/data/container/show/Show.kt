package com.neuronride.android.test.data.container.show

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.neuronride.android.test.data.container.BaseContainer
import com.neuronride.android.test.data.container.ad.VideoAd
import com.neuronride.android.test.data.container.image.Image
import com.neuronride.android.test.data.container.video.Video

const val TABLE_NAME = "container_show_table"

@Entity(tableName = TABLE_NAME)
data class Show(

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    var id: Long,

    @ColumnInfo(name = "channel_image_url")
    var channelImageUrl: String,

    @ColumnInfo(name = "profile_image_url")
    var profileImageUrl: String,

    @Ignore
    var videos: List<Video>,

    @Ignore
    var images: List<Image>,

    @Ignore
    var videoAds: List<VideoAd>

) : BaseContainer() {
    constructor() : this(0L, "", "", ArrayList(), ArrayList(), ArrayList())
}