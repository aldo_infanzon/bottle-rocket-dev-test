package com.neuronride.android.test.ui.show.detail.ads

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.neuronride.android.test.data.container.ad.VideoAd
import com.neuronride.android.test.repository.DefaultDatabase
import io.reactivex.Flowable

class ShowAdsViewModel(app: Application) : AndroidViewModel(app) {

    private val adsDao = DefaultDatabase
        .getInstance(app)
        .videoAdDao()

    fun getAllAdVideos(showId: Long): Flowable<List<VideoAd>> {
        return adsDao.getAllByShow(showId)
    }

}