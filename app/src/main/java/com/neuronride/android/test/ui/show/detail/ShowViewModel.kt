package com.neuronride.android.test.ui.show.detail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.neuronride.android.test.data.container.show.Show
import com.neuronride.android.test.repository.DefaultDatabase
import io.reactivex.Flowable

class ShowViewModel(app: Application) : AndroidViewModel(app) {

    private val dao = DefaultDatabase
        .getInstance(app)
        .showDao()

    fun getShow(id: Long): Flowable<Show> {
        return dao.findById(id)
    }

}