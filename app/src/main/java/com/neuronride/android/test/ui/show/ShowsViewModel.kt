package com.neuronride.android.test.ui.show

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.neuronride.android.test.data.container.show.Show
import com.neuronride.android.test.repository.DefaultDatabase
import io.reactivex.Flowable

class ShowsViewModel(app: Application) : AndroidViewModel(app) {

    private val dao = DefaultDatabase
        .getInstance(app)
        .showDao()

    val shows: Flowable<List<Show>> = dao.getAll()

}