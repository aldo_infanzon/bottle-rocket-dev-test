package com.neuronride.android.test.ui.show.detail

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.neuronride.android.test.R
import com.neuronride.android.test.ui.BaseActivity
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber


class ShowDetailActivity : BaseActivity() {

    companion object {
        const val SHOW_ID = "SHOW_ID"
    }

    private lateinit var toolbar: Toolbar
    private lateinit var viewPager: ViewPager
    private lateinit var tabs: TabLayout

    private lateinit var viewModel: ShowViewModel
    private lateinit var tabAdapter: ShowDetailTabsAdapter

    private val disposable = CompositeDisposable()

    private var showId: Long = -1

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putLong(SHOW_ID, showId)
        super.onSaveInstanceState(outState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_show_detail)

        toolbar = findViewById(R.id.toolbar)
        viewPager = findViewById(R.id.view_pager)
        tabs = findViewById(R.id.tabs)

        viewModel = ViewModelProviders.of(this).get(ShowViewModel::class.java)

        showId = savedInstanceState?.getLong(SHOW_ID) ?: intent.getLongExtra(SHOW_ID, -1)

        setupView()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onStart() {
        super.onStart()

        disposable.add(
            viewModel.getShow(showId)
                .subscribe({ show ->
                    setToolbar(show.name ?: "")
                }, { throwable ->
                    Timber.e(throwable)
                })
        )
    }

    override fun onDestroy() {
        disposable.clear()
        super.onDestroy()
    }

    private fun setupView() {
        tabAdapter = ShowDetailTabsAdapter(
            supportFragmentManager,
            applicationContext,
            showId
        )
        viewPager.adapter = tabAdapter
        tabs.setupWithViewPager(viewPager)
    }

    private fun setToolbar(title: String) {
        toolbar.title = title
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
    }

}