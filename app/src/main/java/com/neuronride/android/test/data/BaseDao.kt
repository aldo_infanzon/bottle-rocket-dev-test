package com.neuronride.android.test.data

import androidx.room.*

@Dao
interface BaseDao<T> {

    /**
     * Insert an item in the database.
     *
     * @param item the obj to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(item: T): Long

    /**
     * Insert an array of item in the database.
     *
     * @param item the obj to be inserted.
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg item: T)

    /**
     * Update an item from the database.
     *
     * @param item the obj to be updated
     */
    @Update
    fun update(item: T)

    /**
     * Delete an item from the database
     *
     * @param item the obj to be deleted
     */
    @Delete
    fun delete(item: T)

}
