package com.neuronride.android.test.data.container.ad

import androidx.room.Dao
import androidx.room.Query
import com.neuronride.android.test.data.BaseDao
import io.reactivex.Flowable

@Dao
abstract class VideoAdDao : BaseDao<VideoAd> {

    @Query("SELECT * from container_show_video_ad_table ORDER BY name ASC")
    abstract fun getAll(): Flowable<List<VideoAd>>

    @Query("SELECT * from container_show_video_ad_table WHERE show_id = :showId ORDER BY name ASC")
    abstract fun getAllByShow(showId: Long): Flowable<List<VideoAd>>

    @Query("SELECT * from container_show_video_ad_table WHERE id = :id")
    abstract fun findById(id: Long): Flowable<VideoAd>

    @Query("DELETE FROM container_show_video_ad_table")
    abstract fun deleteAll()

}