package com.neuronride.android.test.ui.show.detail.images

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.SimpleDraweeView
import com.neuronride.android.test.R
import com.neuronride.android.test.data.container.image.Image

class ImageViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(
        R.layout.view_tab_list_item_image,
        parent,
        false
    )
) {
    private val nameTextView = itemView.findViewById<TextView>(R.id.text_view_title)
    private val videoImageView = itemView.findViewById<SimpleDraweeView>(R.id.simple_drawee_image)

    private var image: Image? = null

    fun bindItem(image: Image?) {
        this.image = image

        nameTextView.text = image?.name
        videoImageView.setImageURI(image?.url)

        itemView.setOnClickListener { view ->
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(image?.url)
            view.context.startActivity(intent)
        }
    }

}