package com.neuronride.android.test.ui.show.detail.main

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.SimpleDraweeView
import com.neuronride.android.test.R
import com.neuronride.android.test.data.container.video.Video


class VideosViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(
        R.layout.view_tab_list_item_video,
        parent,
        false
    )
) {
    private val nameTextView = itemView.findViewById<TextView>(R.id.text_view_title)
    private val descriptionTextView = itemView.findViewById<TextView>(R.id.text_view_description)
    private val videoImageView = itemView.findViewById<SimpleDraweeView>(R.id.simple_drawee_video)

    private var video: Video? = null

    fun bindVideoItem(video: Video?) {
        this.video = video

        nameTextView.text = video?.name
        descriptionTextView.text = video?.description
        videoImageView.setImageURI(video?.videoImageUrl)

        itemView.setOnClickListener { view ->
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(video?.url)
            view.context.startActivity(intent)
        }
    }

}