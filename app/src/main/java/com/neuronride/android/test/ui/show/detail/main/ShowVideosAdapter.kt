package com.neuronride.android.test.ui.show.detail.main

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.neuronride.android.test.data.container.show.Show
import com.neuronride.android.test.data.container.video.Video

private const val SHOW_TYPE = 1
private const val VIDEO_TYPE = 2

class ShowVideosAdapter(
    private val show: Show,
    private val videosList: MutableList<Video>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun getItemCount(): Int {
        return videosList.count() + 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            SHOW_TYPE -> ShowViewHolder(parent)

            VIDEO_TYPE -> VideosViewHolder(parent)

            else -> throw NotImplementedError()
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ShowViewHolder) {
            holder.bindShowItem(show)
        }

        if (holder is VideosViewHolder) {
            holder.bindVideoItem(videosList[position - 1])
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0)
            SHOW_TYPE
        else
            VIDEO_TYPE
    }

    fun updateItems(items: MutableList<Video>) {
        videosList.clear()
        videosList.addAll(items)
        notifyDataSetChanged()
    }
}
