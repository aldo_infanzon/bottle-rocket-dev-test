package com.neuronride.android.test.ui.show.detail.images

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.neuronride.android.test.data.container.image.Image

class ShowImageAdapter(
    private val imagesList: MutableList<Image>
) : RecyclerView.Adapter<ImageViewHolder>() {

    override fun getItemCount(): Int {
        return imagesList.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            : ImageViewHolder = ImageViewHolder(parent)

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.bindItem(imagesList[position])
    }

    fun updateItems(items: MutableList<Image>) {
        imagesList.clear()
        imagesList.addAll(items)
        notifyDataSetChanged()
    }
}
