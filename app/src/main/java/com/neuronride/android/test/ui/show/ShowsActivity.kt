package com.neuronride.android.test.ui.show

import android.os.Bundle
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.neuronride.android.test.R
import com.neuronride.android.test.ui.BaseActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber


class ShowsActivity : BaseActivity() {

    private lateinit var toolbar: Toolbar
    private lateinit var recyclerView: RecyclerView

    private lateinit var viewModel: ShowsViewModel
    private lateinit var adapter: ShowsAdapter

    private val disposable = CompositeDisposable()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(com.neuronride.android.test.R.layout.activity_shows)

        toolbar = findViewById(R.id.toolbar)
        recyclerView = findViewById(R.id.recycler_view)

        viewModel = ViewModelProviders.of(this).get(ShowsViewModel::class.java)

        setToolbar()
        setRecyclerView()
    }

    override fun onStart() {
        super.onStart()

        disposable.add(
            viewModel.shows
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ shows ->
                    if (!::adapter.isInitialized) {
                        adapter = ShowsAdapter(shows.toMutableList())
                        recyclerView.adapter = adapter
                    } else {
                        adapter.updateItems(shows.toMutableList())
                    }
                }, { throwable ->
                    Timber.e(throwable)
                })
        )
    }

    override fun onDestroy() {
        disposable.clear()
        super.onDestroy()
    }

    private fun setToolbar() {
        toolbar.title = getString(R.string.shows_title)
        setSupportActionBar(toolbar)
    }

    private fun setRecyclerView() {
        val divider = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        divider.setDrawable(ContextCompat.getDrawable(baseContext, R.drawable.row_divider)!!)
        recyclerView.addItemDecoration(divider)
        recyclerView.layoutManager = LinearLayoutManager(this)
    }
}