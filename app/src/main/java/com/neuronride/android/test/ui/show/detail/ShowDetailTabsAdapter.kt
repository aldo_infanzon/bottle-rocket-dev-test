package com.neuronride.android.test.ui.show.detail

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.neuronride.android.test.R
import com.neuronride.android.test.ui.show.detail.ads.ShowAdsFragment
import com.neuronride.android.test.ui.show.detail.images.ShowImagesFragment
import com.neuronride.android.test.ui.show.detail.main.ShowMainFragment

private const val TAB_COUNT = 3

class ShowDetailTabsAdapter constructor(
    fragmentManager: FragmentManager,
    private val context: Context,
    private val showId: Long
) : FragmentStatePagerAdapter(fragmentManager) {

    override fun getCount(): Int = TAB_COUNT

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> ShowMainFragment.newInstance(showId)

            1 -> ShowAdsFragment.newInstance(showId)

            2 -> ShowImagesFragment.newInstance(showId)

            else -> throw UnsupportedOperationException()
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> context.getString(R.string.show_detain_main_tab_title)

            1 -> context.getString(R.string.show_detain_ad_tab_title)

            2 -> context.getString(R.string.show_detain_images_tab_title)

            else -> throw NotImplementedError()
        }
    }
}