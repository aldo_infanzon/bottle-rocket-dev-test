package com.neuronride.android.test.ui.show.detail.images

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.neuronride.android.test.data.container.image.Image
import com.neuronride.android.test.repository.DefaultDatabase
import io.reactivex.Flowable

class ShowImageViewModel(app: Application) : AndroidViewModel(app) {

    private val dao = DefaultDatabase
        .getInstance(app)
        .imageDao()

    fun getAllImages(showId: Long): Flowable<List<Image>> {
        return dao.getAllByShow(showId)
    }

}