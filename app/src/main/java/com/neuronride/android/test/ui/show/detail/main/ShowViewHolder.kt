package com.neuronride.android.test.ui.show.detail.main

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.SimpleDraweeView
import com.neuronride.android.test.R
import com.neuronride.android.test.data.container.show.Show

class ShowViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(
        R.layout.view_tab_item_header_show_detail,
        parent,
        false
    )
) {
    private val nameTextView = itemView.findViewById<TextView>(R.id.text_view_title)
    private val descriptionTextView = itemView.findViewById<TextView>(R.id.text_view_description)
    private val chanelImageView = itemView.findViewById<SimpleDraweeView>(R.id.simple_drawee_channel)
    private val profileImageView = itemView.findViewById<SimpleDraweeView>(R.id.simple_drawee_profile)

    private var show: Show? = null

    fun bindShowItem(show: Show?) {
        this.show = show

        nameTextView.text = show?.name
        descriptionTextView.text = show?.description
        chanelImageView.setImageURI(show?.channelImageUrl)
        profileImageView.setImageURI(show?.profileImageUrl)

    }
}