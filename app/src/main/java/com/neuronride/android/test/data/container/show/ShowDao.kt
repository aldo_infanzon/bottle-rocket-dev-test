package com.neuronride.android.test.data.container.show

import androidx.room.Dao
import androidx.room.Query
import com.neuronride.android.test.data.BaseDao
import io.reactivex.Flowable

@Dao
abstract class ShowDao : BaseDao<Show> {

    @Query("SELECT * from container_show_table ORDER BY name ASC")
    abstract fun getAll(): Flowable<List<Show>>

    @Query("SELECT * from container_show_table WHERE id = :id")
    abstract fun findById(id: Long): Flowable<Show>

    @Query("DELETE FROM container_show_table")
    abstract fun deleteAll()

}