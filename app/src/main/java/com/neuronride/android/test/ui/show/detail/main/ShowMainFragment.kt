package com.neuronride.android.test.ui.show.detail.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.neuronride.android.test.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.zipWith
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

private const val SHOW_ID = "SHOW_ID"

class ShowMainFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView

    private lateinit var viewModel: ShowVideosViewModel
    private lateinit var adapter: ShowVideosAdapter

    private var showId: Long = -1

    private val disposable = CompositeDisposable()

    companion object {

        fun newInstance(showId: Long): ShowMainFragment {
            val fragment = ShowMainFragment()
            val args = Bundle()
            args.putLong(SHOW_ID, showId)
            fragment.arguments = args
            return fragment
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putLong(SHOW_ID, showId)
        super.onSaveInstanceState(outState)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showId = arguments?.getLong(SHOW_ID) ?: savedInstanceState?.getLong(SHOW_ID) ?: showId
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val rootView = inflater.inflate(R.layout.fragment_show_main, container, false)

        recyclerView = rootView.findViewById(R.id.recycler_view)

        viewModel = ViewModelProviders.of(this).get(ShowVideosViewModel::class.java)

        setRecyclerView()

        return rootView
    }

    override fun onStart() {
        super.onStart()

        disposable.add(
            viewModel.getShow(showId)
                .zipWith(viewModel.getAllVideos(showId))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ pair ->
                    if (!::adapter.isInitialized) {
                        adapter = ShowVideosAdapter(pair.first, pair.second.toMutableList())
                        recyclerView.adapter = adapter
                    } else {
                        adapter.updateItems(pair.second.toMutableList())
                    }
                }, { throwable ->
                    Timber.e(throwable)
                })
        )
    }

    override fun onDestroy() {
        disposable.clear()
        super.onDestroy()
    }

    private fun setRecyclerView() {
        recyclerView.layoutManager = LinearLayoutManager(activity)
    }
}