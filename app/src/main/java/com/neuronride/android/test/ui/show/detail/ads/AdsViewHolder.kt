package com.neuronride.android.test.ui.show.detail.ads

import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.SimpleDraweeView
import com.neuronride.android.test.R
import com.neuronride.android.test.data.container.ad.VideoAd

class AdsViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(
        R.layout.view_tab_list_item_video,
        parent,
        false
    )
) {
    private val nameTextView = itemView.findViewById<TextView>(R.id.text_view_title)
    private val descriptionTextView = itemView.findViewById<TextView>(R.id.text_view_description)
    private val videoImageView = itemView.findViewById<SimpleDraweeView>(R.id.simple_drawee_video)

    private var videoAd: VideoAd? = null

    fun bindItem(videoAd: VideoAd?) {
        this.videoAd = videoAd

        nameTextView.text = videoAd?.name
        descriptionTextView.text = videoAd?.productDescription
        videoImageView.setImageURI(videoAd?.videoImageUrl)

        itemView.setOnClickListener { view ->
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(videoAd?.url)
            view.context.startActivity(intent)
        }
    }

}