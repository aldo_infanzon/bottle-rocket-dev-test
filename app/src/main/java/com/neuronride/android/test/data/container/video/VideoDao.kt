package com.neuronride.android.test.data.container.video

import androidx.room.Dao
import androidx.room.Query
import com.neuronride.android.test.data.BaseDao
import io.reactivex.Flowable

@Dao
abstract class VideoDao : BaseDao<Video> {

    @Query("SELECT * from container_show_video_table ORDER BY name ASC")
    abstract fun getAll(): Flowable<List<Video>>

    @Query("SELECT * from container_show_video_table WHERE show_id = :showId ORDER BY name ASC")
    abstract fun getAllByShow(showId: Long): Flowable<List<Video>>

    @Query("SELECT * from container_show_video_table WHERE id = :id")
    abstract fun findById(id: Long): Flowable<Video>

    @Query("DELETE FROM container_show_video_table")
    abstract fun deleteAll()

}