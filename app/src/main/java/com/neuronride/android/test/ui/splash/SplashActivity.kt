package com.neuronride.android.test.ui.splash

import android.content.Intent
import android.os.Bundle
import com.neuronride.android.test.data.request.ShowsRequestModel
import com.neuronride.android.test.repository.DefaultDatabase
import com.neuronride.android.test.ui.BaseActivity
import com.neuronride.android.test.ui.show.ShowsActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import timber.log.Timber

class SplashActivity : BaseActivity() {

    private val disposables = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Start Database

        disposables.add(
            ShowsRequestModel.fromJson(this)
                .flatMap { shows ->
                    DefaultDatabase.fillDb(applicationContext, shows)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    val showsIntent = Intent(this, ShowsActivity::class.java)
                    startActivity(showsIntent)
                    finish()
                }, { throwable ->
                    Timber.e(throwable)
                })
        )
    }

    override fun onDestroy() {
        disposables.dispose()
        super.onDestroy()
    }

}