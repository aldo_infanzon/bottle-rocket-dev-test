package com.neuronride.android.test.data.container.video

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.neuronride.android.test.data.container.BaseAsset
import com.neuronride.android.test.data.container.show.Show

const val TABLE_NAME = "container_show_video_table"

@Entity(
    tableName = TABLE_NAME,
    foreignKeys = [
        ForeignKey(
            entity = Show::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("show_id")
        )
    ]
)
data class Video(

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    var id: Long,

    @ColumnInfo(name = "show_id")
    var showId: Long,

    @ColumnInfo(name = "expiration_date")
    var expirationDate: Long,

    var description: String? = null,

    @ColumnInfo(name = "video_image_url")
    var videoImageUrl: String,

    //Videos have a field that indicates if it is a movie, a full episode or a clip.
    @ColumnInfo(name = "content_type")
    var contentType: String

) : BaseAsset()