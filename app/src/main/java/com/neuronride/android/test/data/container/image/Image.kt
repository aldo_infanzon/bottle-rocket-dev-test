package com.neuronride.android.test.data.container.image

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.neuronride.android.test.data.container.BaseAsset
import com.neuronride.android.test.data.container.show.Show

const val TABLE_NAME = "container_show_image_table"

@Entity(
    tableName = TABLE_NAME,
    foreignKeys = [
        ForeignKey(
            entity = Show::class,
            parentColumns = arrayOf("id"),
            childColumns = arrayOf("show_id")
        )
    ]
)
data class Image(

    @PrimaryKey
    @NonNull
    var id: Long,

    @ColumnInfo(name = "show_id")
    var showId: Long

) : BaseAsset()