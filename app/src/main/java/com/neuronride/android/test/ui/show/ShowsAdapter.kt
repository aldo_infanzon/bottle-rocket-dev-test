package com.neuronride.android.test.ui.show

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.neuronride.android.test.data.container.show.Show

class ShowsAdapter(
    private val showList: MutableList<Show>
) : RecyclerView.Adapter<ShowsViewHolder>() {

    override fun getItemCount(): Int {
        return showList.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            : ShowsViewHolder = ShowsViewHolder(parent)

    override fun onBindViewHolder(holder: ShowsViewHolder, position: Int) {
        holder.bindItem(showList[position])
    }

    fun updateItems(items: MutableList<Show>) {
        showList.clear()
        showList.addAll(items)
        notifyDataSetChanged()
    }
}
