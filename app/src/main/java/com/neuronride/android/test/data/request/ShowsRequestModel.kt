package com.neuronride.android.test.data.request

import android.content.Context
import com.google.gson.Gson
import com.neuronride.android.test.R
import com.neuronride.android.test.data.container.show.Show
import io.reactivex.Single
import java.io.InputStream

class ShowsRequestModel {

    companion object {

        fun fromJson(context: Context): Single<ShowsRequestModel> {
            return Single.just(Gson())
                .flatMap { gson ->

                    val inputStream: InputStream =
                        context.resources.openRawResource(R.raw.dummy_container_data)

                    val jsonString = inputStream.bufferedReader()
                        .use { text ->
                            text.readText()
                        }

                    Single.just(gson.fromJson(jsonString, ShowsRequestModel::class.java))
                }
        }
    }

    var shows: List<Show>? = null

}
