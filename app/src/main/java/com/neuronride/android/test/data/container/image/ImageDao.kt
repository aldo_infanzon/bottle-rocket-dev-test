package com.neuronride.android.test.data.container.image

import androidx.room.Dao
import androidx.room.Query
import com.neuronride.android.test.data.BaseDao
import io.reactivex.Flowable

@Dao
abstract class ImageDao : BaseDao<Image> {

    @Query("SELECT * from container_show_image_table ORDER BY name ASC")
    abstract fun getAll(): Flowable<List<Image>>

    @Query("SELECT * from container_show_image_table WHERE show_id = :showId ORDER BY name ASC")
    abstract fun getAllByShow(showId: Long): Flowable<List<Image>>

    @Query("SELECT * from container_show_image_table WHERE id = :id")
    abstract fun findById(id: Long): Flowable<Image>

    @Query("DELETE FROM container_show_image_table")
    abstract fun deleteAll()

}