package com.neuronride.android.test.data.container

open class BaseAsset {

    var name: String? = null

    var type: String? = null

    var url: String? = null
}