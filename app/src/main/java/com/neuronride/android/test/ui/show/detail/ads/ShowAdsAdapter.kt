package com.neuronride.android.test.ui.show.detail.ads

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.neuronride.android.test.data.container.ad.VideoAd

class ShowAdsAdapter(
    private val adsList: MutableList<VideoAd>
) : RecyclerView.Adapter<AdsViewHolder>() {

    override fun getItemCount(): Int {
        return adsList.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)
            : AdsViewHolder = AdsViewHolder(parent)

    override fun onBindViewHolder(holder: AdsViewHolder, position: Int) {
        holder.bindItem(adsList[position])
    }

    fun updateItems(items: MutableList<VideoAd>) {
        adsList.clear()
        adsList.addAll(items)
        notifyDataSetChanged()
    }
}
