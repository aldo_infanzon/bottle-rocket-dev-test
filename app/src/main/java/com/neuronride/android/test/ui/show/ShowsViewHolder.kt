package com.neuronride.android.test.ui.show

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.SimpleDraweeView
import com.neuronride.android.test.R
import com.neuronride.android.test.data.container.show.Show
import com.neuronride.android.test.ui.show.detail.ShowDetailActivity

class ShowsViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(
        R.layout.view_list_item_show,
        parent,
        false
    )
) {
    private val nameTextView = itemView.findViewById<TextView>(R.id.text_view_title)
    private val chanelImageView = itemView.findViewById<SimpleDraweeView>(R.id.simple_drawee_channel)
    private val profileImageView = itemView.findViewById<SimpleDraweeView>(R.id.simple_drawee_profile)

    private var show: Show? = null

    fun bindItem(show: Show?) {

        this.show = show

        nameTextView.text = show?.name
        chanelImageView.setImageURI(show?.channelImageUrl)
        profileImageView.setImageURI(show?.profileImageUrl)

        itemView.setOnClickListener { view ->
            val showDetailIntent = Intent(view.context, ShowDetailActivity::class.java)
            showDetailIntent.putExtra(ShowDetailActivity.SHOW_ID, show?.id)
            view.context.startActivity(showDetailIntent)
        }
    }
}